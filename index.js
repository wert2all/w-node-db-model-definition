'use strict';

/**
 * The entry point.
 *
 * @module DBModelDefinition
 */
module.exports = require('./src/ModelDefinitionBuider');
module.exports.interface = require('./src/Interfaces/ModelDefinitionInterface');
module.exports.relation = require('./src/ModelRelationFactory');

