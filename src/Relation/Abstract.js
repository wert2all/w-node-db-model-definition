'use strict';
const DBModelRelationInterface = require('../Interfaces/ModelRelationInterface');

/**
 * @class DBModelRelationAbstract
 * @type {DBModelRelationAbstract}
 * @extends {DBModelRelationInterface}
 *
 * @abstract
 */
class DBModelRelationAbstract extends DBModelRelationInterface {
    /**
     * @param {DBModelDefinitionInterface} modelDefinition
     * @param {string} childrenAlias
     */
    constructor(modelDefinition, childrenAlias) {
        super();
        /**
         *
         * @type {DBModelDefinitionInterface}
         * @protected
         */
        this._modelDefinition = modelDefinition;
        /**
         * @type {string}
         * @protected
         */
        this._childrenAlias = childrenAlias;
    }

    /**
     * @return {DBModelDefinitionInterface}
     */
    getDefinition() {
        return this._modelDefinition;
    }

    /**
     * @return {string}
     */
    getRelationAlias() {
        return this._childrenAlias;
    }
}


module.exports = DBModelRelationAbstract;
