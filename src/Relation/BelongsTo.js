'use strict';

const DBModelRelationAbstract = require('./Abstract');

/**
 * @class DBModelRelationBelongsTo
 * @type {DBModelRelationInterface}
 * @extends {DBModelRelationAbstract}
 */
class DBModelRelationBelongsTo extends DBModelRelationAbstract {

}

module.exports = DBModelRelationBelongsTo;
