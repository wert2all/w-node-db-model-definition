'use strict';

const DBModelRelationAbstract = require('./Abstract');

/**
 * @class DBModelRelationHasMany
 * @type {DBModelRelationInterface}
 * @extends {DBModelRelationAbstract}
 */
class DBModelRelationHasMany extends DBModelRelationAbstract {
    /**
     * @return {string}
     */
    getRelationType() {
        return 'hasMany';
    }
}

module.exports = DBModelRelationHasMany;
