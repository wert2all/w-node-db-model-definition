'use strict';
const DBModelDefinitionImplementationError = require('./../Error/ImplementationError');

/**
 * @class DBModelColumnDefinitionInterface
 * @type DBModelColumnDefinitionInterface
 */
class DBModelColumnDefinitionInterface {

    /**
     * @return {string}
     * @abstract
     */
    getName() {
        throw new DBModelDefinitionImplementationError(this, 'getName');
    }

    /**
     * @return {string}
     * @abstract
     */
    getType() {
        throw new DBModelDefinitionImplementationError(this, 'getType');
    }

    /**
     * @return {*}
     * @abstract
     */
    getTypeOptions() {
        throw new DBModelDefinitionImplementationError(this, 'getTypeOptions');
    }

    /**
     * @return {int}
     * @abstract
     */
    getSize() {
        throw new DBModelDefinitionImplementationError(this, 'getType');
    }

    /**
     * @return {int|null}
     * @abstract
     */
    getDecimal() {
        throw new DBModelDefinitionImplementationError(this, 'getDecimal');
    }

    /**
     * @return {string|int}
     * @abstract
     */
    getDefaultValue() {
        throw new DBModelDefinitionImplementationError(this, 'getDefaultValue');
    }

    /**
     * @return {boolean}
     * @abstract
     */
    isAllowNull() {
        throw new DBModelDefinitionImplementationError(this, 'isAllowNull');
    }
}

module.exports = DBModelColumnDefinitionInterface;
