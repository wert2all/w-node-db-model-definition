'use strict';
const DBModelDefinitionImplementationError = require('./../Error/ImplementationError');

/**
 * @class DBModelIndexDefinitionInterface
 * @type DBModelIndexDefinitionInterface
 */
class DBModelIndexDefinitionInterface {
    /**
     * @return {boolean}
     * @abstract
     */
    isUnique() {
        throw new DBModelDefinitionImplementationError(this, 'isUnique');
    }

    /**
     * @return {[string]}
     * @abstract
     */
    getFieldsNames() {
        throw new DBModelDefinitionImplementationError(this, 'getFieldsNames');
    }
}

module.exports = DBModelIndexDefinitionInterface;
