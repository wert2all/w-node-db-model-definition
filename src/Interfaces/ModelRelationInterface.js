'use strict';
const ThrowableImplementationError = require('w-node-implementation-error');

/**
 * @class DBModelRelationInterface
 * @type {DBModelRelationInterface}
 */
class DBModelRelationInterface {

    /**
     * @return {DBModelDefinitionInterface}
     * @abstract
     */
    getDefinition() {
        throw  new ThrowableImplementationError(this, 'getDefinition');
    }

    /**
     * @return {string}
     * @abstract
     */
    getRelationAlias() {
        throw  new ThrowableImplementationError(this, 'getRelationAlias');
    }

    /**
     * @return {string}
     * @abstract
     */
    getRelationType(){
        throw new ThrowableImplementationError(this, 'getRelationType');
    }
}

module.exports = DBModelRelationInterface;
