'use strict';

const DBModelDefinitionImplementationError = require('../Error/ImplementationError');

/**
 * @class DBModelDefinitionInterface
 * @type DBModelDefinitionInterface
 * @interface
 */
class DBModelDefinitionInterface {
    /**
     * @return {string}
     * @abstract
     */
    getModelName() {
        throw new DBModelDefinitionImplementationError(this, 'getName');
    }

    /**
     * @return {[DBModelIndexDefinitionInterface]}
     * @abstract
     */
    getIndexes() {
        throw new DBModelDefinitionImplementationError(this, 'getIndexes');
    }

    /**
     * @return {[DBModelColumnDefinitionInterface]}
     * @abstract
     */
    getColumns() {
        throw new DBModelDefinitionImplementationError(this, 'getColumns');
    }

    /**
     * @return {DBModelRelationInterface|null}
     * @abstract
     */
    getRelation() {
        throw new DBModelDefinitionImplementationError(this, 'getRelation');
    }

    /**
     * @return {string}
     * @abstract
     */
    getPrimaryColumnName() {
        throw new DBModelDefinitionImplementationError(this, 'getPrimaryColumnName');
    }
}

module.exports = DBModelDefinitionInterface;
