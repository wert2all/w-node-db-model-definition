'use strict';

const ThrowableImplementationError = require('w-node-implementation-error');
/**
 * @class DBModelDefinitionImplementationError
 * @type {DBModelDefinitionImplementationError}
 * @extends {ThrowableImplementationError}
 */
module.exports = class DBModelDefinitionImplementationError
    extends ThrowableImplementationError {
};
