'use strict';
const DBModelColumnDefinitionInterface =
    require('../Interfaces/ModelColumnDefinitionInterface');

/**
 * @class ColumnImplementation
 * @type DBModelColumnDefinitionInterface
 */
class ColumnImplementation extends DBModelColumnDefinitionInterface {

    /**
     *
     * @param {string} name
     * @param {WGDataInterface} data
     */
    constructor(name, data) {
        super();
        /**
         *
         * @type {string}
         * @private
         */
        this._name = name;

        /**
         *
         * @type {WGDataInterface}
         * @private
         */
        this._data = data;
    }

    /**
     * @return {string}
     */
    getName() {
        return this._name;
    }

    /**
     * @return {string|int}
     */
    getDefaultValue() {
        return this._data.getData('defaultValue');
    }

    /**
     * @return {int}
     */
    getSize() {
        return this._data.getData('size');
    }

    /**
     * @return {string}
     */
    getType() {
        return this._data.getData('type');
    }

    /**
     * @return {*}
     */
    getTypeOptions() {
        return this._data.getData('typeOption');
    }

    /**
     * @return {boolean}
     */
    isAllowNull() {
        return (this._data.getData('isAllowed') === true);
    }

    /**
     *
     * @return {int|null}
     */
    getDecimal() {
        return this._data.getData('decimal');
    }
}

module.exports = ColumnImplementation;
