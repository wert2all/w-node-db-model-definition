'use strict';
const DBModelIndexDefinitionInterface =
    require('../Interfaces/ModelIndexDefinitionInterface');

/**
 * @type {DBModelIndexDefinitionInterface}
 */
class IndexImplementation extends DBModelIndexDefinitionInterface {

    /**
     *
     * @param {[]} fields
     * @param {boolean} isUnique
     */
    constructor(fields, isUnique = false) {
        super();
        /**
         *
         * @type {boolean}
         * @private
         */
        this._isUnique = isUnique;
        /**
         *
         * @type {[]}
         * @private
         */
        this._fields = fields;
    }

    /**
     *
     * @return {[]}
     */
    getFieldsNames() {
        return this._fields;
    }

    /**
     *
     * @return {boolean}
     */
    isUnique() {
        return this._isUnique;
    }
}

module.exports = IndexImplementation;
