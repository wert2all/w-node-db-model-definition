'use strict';
const DBModelDefinitionInterface = require('../Interfaces/ModelDefinitionInterface');

/**
 * @class DBModelDefinitionImplementation
 * @type DBModelDefinitionInterface
 */
class DBModelDefinitionImplementation extends DBModelDefinitionInterface {

    /**
     *
     * @param {string} name
     * @param {[DBModelColumnDefinitionInterface]} columns
     * @param {[DBModelIndexDefinitionInterface]} indexes
     * @param {DBModelRelationInterface} relation
     */
    constructor(name, columns, indexes, relation) {
        super();
        /**
         *
         * @type {string}
         * @private
         */
        this._name = name;

        /**
         *
         * @type {[DBModelColumnDefinitionInterface]}
         * @private
         */
        this._columns = columns;
        /**
         *
         * @type {[DBModelIndexDefinitionInterface]}
         * @private
         */
        this._indexes = indexes;
        /**
         * @type {DBModelRelationInterface|null}
         * @private
         */
        this._relation = relation;
    }

    /**
     *
     * @return {string}
     */
    getModelName() {
        return this._name;
    }

    /**
     * @return {[DBModelIndexDefinitionInterface]}
     * @abstract
     */
    getIndexes() {
        return this._indexes;
    }

    /**
     * @return {[DBModelColumnDefinitionInterface]}
     */
    getColumns() {
        return this._columns;
    }

    /**
     * @return {DBModelRelationInterface|null}
     */
    getRelation() {
        return this._relation;
    }

    /**
     *
     * @param {DBModelRelationInterface} relation
     * @return {DBModelDefinitionInterface}
     */
    setRelation(relation) {
        this._relation = relation;
        return this;
    }

    /**
     * @return {string}
     */
    getPrimaryColumnName() {
        return 'id';
    }
}

module.exports = DBModelDefinitionImplementation;
