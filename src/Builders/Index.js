'use strict';
const IndexImplementation = require('./../Implementations/Index');

class DBIndexBuilder {
    constructor() {
        /**
         *
         * @type {[DBModelColumnDefinitionInterface]}
         * @private
         */
        this._fields = [];
        this._isUnique = false;
    }

    /**
     *
     * @param {boolean} isUnique
     * @return {DBIndexBuilder}
     */
    setUnique(isUnique = false) {
        this._isUnique = isUnique;
        return this;
    }

    /**
     *
     * @param {[]} fields
     * @return {DBIndexBuilder}
     */
    setFields(fields = []) {
        this._fields = Array.isArray(fields) ? fields : [];
        return this;
    }

    /**
     *
     * @return {DBModelIndexDefinitionInterface}
     */
    build() {
        return new IndexImplementation(this._fields, this._isUnique);
    }
}

module.exports = DBIndexBuilder;
