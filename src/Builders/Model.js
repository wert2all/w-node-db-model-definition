'use strict';
const ModelImplementation = require('./../Implementations/Model');

/**
 * @class DBModelBuilder
 * @type DBModelBuilder
 */
class DBModelBuilder {

    constructor() {
        /**
         *
         * @type {[DBModelColumnDefinitionInterface]}
         * @private
         */
        this._columns = [];
        /**
         *
         * @type {[DBModelIndexDefinitionInterface]}
         * @private
         */
        this._indexes = [];
        /**
         * @type {DBModelRelationInterface|null}
         * @private
         */
        this._relation = null;
    }

    /**
     *
     * @param {DBModelColumnDefinitionInterface} column
     * @return DBModelBuilder
     */
    addColumn(column) {
        this._columns.push(column);
        return this;
    }

    /**
     *
     * @param {DBModelIndexDefinitionInterface} index
     * @return DBModelBuilder
     */
    addIndex(index) {
        this._indexes.push(index);
        return this;
    }

    /**
     *
     * @param {DBModelRelationInterface} relation
     * @return {DBModelBuilder}
     */
    setRelation(relation) {
        this._relation = relation;
        return this;
    }

    build(name) {
        let model = new ModelImplementation(
            name,
            this._columns,
            this._indexes,
            this._relation
        );
        return model;
    }
}

module.exports = DBModelBuilder;
