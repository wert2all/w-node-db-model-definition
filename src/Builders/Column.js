'use strict';
/** @type {WGDataInterface} */
const WGData = require('w-node-data-value');
const ColumnImplementation = require('./../Implementations/Column');

class DBColumnBuilder {
    constructor() {
        /**
         *
         * @type {WGDataInterface}
         * @private
         */
        this._data = new WGData({
            isAllowed: true,
            size: null,
            type: 'string'
        });
    }

    /**
     *
     * @param {boolean} isAllowed
     * @return {DBColumnBuilder}
     */
    setAllowed(isAllowed) {
        this._data = this._data.setData('isAllowed', isAllowed);
        return this;
    }

    /**
     *
     * @param {int} size
     * @param {int|null} decimal
     * @return {DBColumnBuilder}
     */
    setSize(size, decimal = null) {
        this._data = this._data.setData('size', size);
        this._data = this._data.setData('decimal', decimal);
        return this;
    }

    /**
     *
     * @param {string} type
     * @param {*} typeOption
     * @return {DBColumnBuilder}
     */
    setType(type, typeOption = null) {
        this._data = this._data.setData('type', type);
        this._data = this._data.setData('typeOption', typeOption);
        return this;
    }

    /**
     *
     * @param {*} defaultValue
     * @return {DBColumnBuilder}
     */
    setDefault(defaultValue) {
        this._data = this._data.setData('defaultValue', defaultValue);
        return this;
    }

    /**
     *
     * @param {string} name
     * @return {DBModelColumnDefinitionInterface}
     */
    build(name) {
        return new ColumnImplementation(name, this._data);
    }
}

module.exports = DBColumnBuilder;
