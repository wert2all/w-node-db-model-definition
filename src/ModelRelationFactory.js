'use strict';
const DBModelRelationHasMany = require('./Relation/HasMany');

/**
 * @class DBModelRelationFactory
 * @type {DBModelRelationFactory}
 */
class DBModelRelationFactory {
    /**
     *
     * @param {DBModelDefinitionInterface} modelDefinition
     * @param {string} childrenAlias
     */
    many(modelDefinition, childrenAlias) {
        return new DBModelRelationHasMany(modelDefinition, childrenAlias);
    }
}

module.exports = DBModelRelationFactory;
