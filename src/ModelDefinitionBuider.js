'use strict';

const DBModelBuilder = require('./Builders/Model');
const DBColumnBuilder = require('./Builders/Column');
const DBIndexBuilder = require('./Builders/Index');

/**
 * @class DBModelDefinitionBuilder
 * @type DBModelDefinitionBuilder
 *
 */
class DBModelDefinitionBuilder {
    /**
     * @return DBModelBuilder
     */
    model() {
        return new DBModelBuilder();
    }

    /**
     *
     * @return {DBColumnBuilder}
     */
    column() {
        return new DBColumnBuilder();
    }

    /**
     *
     * @return {DBIndexBuilder}
     */
    index() {
        return new DBIndexBuilder();
    }
}

module.exports = DBModelDefinitionBuilder;
